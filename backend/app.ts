require('dotenv').config()
import express, { Application, Request, Response } from 'express'
import routes from './src/api/routes/index'
import dbInit from './src/database/init'

dbInit()

const PORT: number = 8000;

export const get = () => {
    const app: Application = express()

    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    
    app.get('/', async(_req: Request, res: Response): Promise<Response> => {
        return res.status(200).send({ message: `Welcome to the images API! \n Endpoints available at http://localhost:${PORT}/api` })
    })
    
    app.use('/api', routes)

    app.get('*', function(_req, res){
        res.status(404).send('You have nothing to do here!!');
    });
    return app
}

export const start = () => {
    const app = get()
    try {
        app.listen(PORT, () => {
            console.log(`Server running on http://localhost:${PORT}`)
        })
    } catch (error: any) {
        console.log(`Error occurred: ${error.message}`)
    }
}

start()