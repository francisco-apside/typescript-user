export interface User {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    phone: number;
    isActive: boolean;
    createdAt: Date;
    updatedAt: Date;
}