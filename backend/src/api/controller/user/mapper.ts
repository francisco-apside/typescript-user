import { User } from '@/api/interfaces'
import { UserOuput } from '@/database/models/User'

export const toUser = (user: UserOuput): User => {
    return {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        address: user.address,
        phone: user.phone,
        isActive: user.isActive,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
    }
}
