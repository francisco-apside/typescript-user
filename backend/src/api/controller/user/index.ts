import * as service from '../../../database/services/User'
import { CreateUserDTO, FilterUsersDTO } from '@/api/dto/user.dto'
import { User } from '@/api/interfaces'
import * as mapper from './mapper'

export const create = async(payload: CreateUserDTO): Promise<User> => {
    return mapper.toUser(await service.create(payload))
}

export const deleteById = async(id: number): Promise<Boolean> => {
    const isDeleted = await service.deleteById(id)
    return isDeleted
}

export const getAll = async(filters: FilterUsersDTO): Promise<User[]> => {
    return (await service.getAll(filters)).map(mapper.toUser)
}