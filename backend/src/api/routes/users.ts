import { Router, Request, Response } from "express"
import * as userController from '../controller/user'
import { FilterUsersDTO, CreateUserDTO } from "../dto/user.dto"

const userRouter = Router()

userRouter.post('/findAll', async (req: Request, res: Response) => {
    const filters:FilterUsersDTO = req.body

    const results = await userController.getAll(filters)
    return res.status(200).send(results)
})

userRouter.post('/create', async (req: Request, res: Response) => {
    const payload:CreateUserDTO = req.body

    const result = await userController.create(payload)
    return res.status(200).send(result)
})

userRouter.post('/delete', async (req: Request, res: Response) => {
    const id = Number(req.body.id)

    const result = await userController.deleteById(id)
    return res.status(204).send({
        success: result
    })
})

export default userRouter