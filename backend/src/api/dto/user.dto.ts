import { FILTER_SELECT_DB, FILTER_SELECT_BOTH, FILTER_SELECT_API } from "@/utils/filtersConst";

export type CreateUserDTO = {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    phone: number;
    isActive: boolean;
    imageId: number;
}

export type FilterUsersDTO = {
    firstName?: string,
    typeSearch?: TypeSearch
}

export type TypeSearch = typeof FILTER_SELECT_DB | typeof FILTER_SELECT_BOTH | typeof FILTER_SELECT_API