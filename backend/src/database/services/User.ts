import * as userDal from '../dal/user'
import { GetAllUsersFilters } from '../dal/types'
import {UserInput, UserOuput} from '../models/User'

export const create = async (payload: UserInput): Promise<UserOuput> => {
    return userDal.create(payload)
}

export const deleteById = (id: number): Promise<boolean> => {
    return userDal.deleteById(id)
}

export const getAll = (filters: GetAllUsersFilters): Promise<UserOuput[]> => {
    return userDal.getAll(filters)
}