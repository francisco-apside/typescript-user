interface ListFilters {
    firstName?: string
}

export interface GetAllUsersFilters extends ListFilters {}
