import { Op } from 'sequelize'
import { User } from '../models'
import { GetAllUsersFilters } from './types'
import { UserInput, UserOuput } from '../models/User'

export const create = async (payload: UserInput): Promise<UserOuput> => {
    const user = await User.create(payload)
    return user
}

export const deleteById = async (id: number): Promise<boolean> => {
    const deletedUserCount = await User.destroy({
        where: {id}
    })
    return !!deletedUserCount
}

export const getAll = async (filters: GetAllUsersFilters): Promise<UserOuput[]> => {
    return User.findAll({
        where: {
            ...(filters?.firstName && {firstName: {[Op.eq]: filters.firstName}})
        }
    })
}